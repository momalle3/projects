Project 01 - Grading
====================

**Score**: 13.75 / 20

Deductions
----------

* Thor

    - 0.25  Doesn't set host properly (uses address instead)
    - 0.25  Need to use HTTP/1.0 otherwise client hangs
    - 0.25  Does not handle -r and -p flags properly
    - 1.0   Not fully concurrent
            Should fork once per process, not per request
            Should fork all children and then wait
    - 0.5   Child needs to exit after all requests

* Spidey

    - 2.0   Fails all tests (hangs)
    - 1.0   Does not implement forking
    - 0.5   Handle file should use self.uripath, include mimetype
    - 0.5   Does not parse all HTTP headers

* Report
    
    *       Incomplete due to lack of implementation

Comments
--------
