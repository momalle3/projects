#!/usr/bin/env python2.7

import getopt
import logging
import os
import socket
import sys
import time

# Constants

PROGRAM  = os.path.basename(sys.argv[0])
LOGLEVEL = logging.INFO
PORT     = 9234
REQUESTS = 1
PROCESSES = 1

# Utility Functions

def usage(exit_code=0):
	print >>sys.stderr, '''Usage: {program} URL [-r REQUESTS -p PROCESSES -v] 

Options:

    -h       Show this help message
    -v       Set logging to DEBUG level
    
    -r REQUESTS  Number of requests per process (default is 1)
    -p PROCESSES Number of processes (default is 1)
'''.format(port=PORT, program=PROGRAM)
	sys.exit(exit_code)

# TCPClient Class

class TCPClient(object):
	def __init__(self, address, port):
		self.logger  = logging.getLogger()
		self.address = address
		self.port	 = port
		self.socket  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	def handle(self):
		self.stream.write('Hello\n')
		self.logger.debug('Handle')
		raise NotImplementedError

	def run(self):
		try:
			self.socket.connect((self.address, self.port))
			self.stream = self.socket.makefile('w+')
		except socket.error as e:
			self.logger.error('Could not connect to {}:{}: {}'.format(self.address, self.port, e))
			sys.exit(1)

		self.logger.debug('Connected to {}:{}...'.format(self.address, self.port))
		try:
			self.handle()
		except Exception as e:
			self.logger.exception('Exception: {}', e)
		finally:
			self.finish()

	def finish(self):
		self.logger.debug('Finish')
		try:
			self.socket.shutdown(socket.SHUT_RDWR)
		except socket.error:
			pass
		finally:
			self.socket.close()
			
# HTTPClient Class

class HTTPClient(TCPClient):
	def __init__(self, address, port, path):
		TCPClient.__init__(self, address, port)
		self.host = address
		self.path = path

	def handle(self):
		self.logger.debug('Handle')
		
		# Send request
		self.stream.write('GET {} HTTP/1.1\r\n'.format(self.path))
		self.stream.write('Host: {}\r\n'.format(self.host))
		self.stream.write('\r\n')
		self.stream.flush()

		try:
			# Receive response
			data = self.stream.readline()
			while data:
				data = self.stream.readline()
				sys.stdout.write(data)   
		except socket.error:
			pass    # Ignore socket errors

# Main Execution

if __name__ == '__main__':

    # Parse command-line arguments
	try:
		options, arguments = getopt.getopt(sys.argv[1:], "hvrp")
	except getopt.GetoptError as e:
		print e
		usage(1)

	for opts, args in options:
		if opts == '-v':
			LOGLEVEL = logging.DEBUG
		if opts == '-r':
			REQUESTS = args
		if opts == '-p':
			PROCESSES= args
			
	if len(arguments) > 0:
		url = arguments[0]
	else:
		usage(1)
		
	# Parse URL
	url = url.split('://')[-1]
	if '/' not in url:
		path = '/'
	else:
		path = '/' + url.split('/', 1)[-1]
		url = url.split('/')[-2]
	if '?' in path:
		path = path.split('?')[-2]
	if ':' not in url:
		port = 80
	else:
		port = int(url.split(':', 1)[-1])
		url = url.split(':')[-2]
	address = url
	
	# Set logging level
	logging.basicConfig(
		level   = LOGLEVEL,
		format  = '[%(asctime)s] %(message)s',
		datefmt = '%Y-%m-%d %H:%M:%S',
	)
	try:
		address = socket.gethostbyname(address)
	except socket.gaierror as e:
		logging.error('Unable to lookup {}: {}'.format(address, e))
		sys.exit(1)

	# Execute
	status = 0
	for process in range(int(PROCESSES)):
		total_time = 0
		for request in range(int(REQUESTS)):
			start_time = time.time()
			client = HTTPClient(address, port, path)
			try:
				pid = os.fork()
			except OSError as e:
				print >> sys.stderr, 'Unable to fork: {}'.format(e)
				sys.exit(1)
			if pid == 0:
				try:
					client.run()
				except KeyboardInterrupt:
					sys.exit(0)
			else:
				pid, status = os.wait()
				end_time = time.time()
				length = end_time-start_time
				client.logger.debug('{} | Elapsed time: {:.2f} seconds'.format(pid,length))
				total_time += length
		average = float(total_time)/(request+1)
		client.logger.debug('{} | Average elapsed time: {:.2f} seconds'.format(pid, average))
	client.logger.debug('Process {} terminated with exit status {}'.format(pid, status))
	
	
