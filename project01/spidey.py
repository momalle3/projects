#!/usr/bin/env python2.7

import getopt
import logging
import os
import socket
import sys

# Constants

ADDRESS  = '0.0.0.0'
PORT     = 9234
BACKLOG  = 0
LOGLEVEL = logging.INFO
DOCROOT  = '.'
FORK 	 = False
PROGRAM  = os.path.basename(sys.argv[0])

# Utility Functions

def usage(exit_code=0):
	print >>sys.stderr, '''Usage: {program} [-d DOCROOT -p PORT -f -v]

Options:

    -h         Show this help message
    -f         Enable forking mode
    -v         Set logging to DEBUG level

    -d DOCROOT Set root directory (default is current directory)
    -p PORT    TCP Port to listen to (default is 9234)
'''.format(port=PORT, program=PROGRAM)
	sys.exit(exit_code)


# Base Handler		

class BaseHandler(object):
     def __init__(self, fd, address):
        ''' Construct handler from file descriptor and remote client address '''
        self.logger  = logging.getLogger()        # Grab logging instance
        self.socket  = fd                         # Store socket file descriptor
        self.address = '{}:{}'.format(*address)   # Store address
        self.stream  = self.socket.makefile('w+') # Open file object from file descriptor

        self.debug('Connect')

	def debug(self, message, *args):
		''' Convenience debugging function '''
		message = message.format(*args)
		self.logger.debug('{} | {}'.format(self.address, message))

	def info(self, message, *args):
		''' Convenience information function '''
		message = message.format(*args)
		self.logger.info('{} | {}'.format(self.address, message))

	def warn(self, message, *args):
		''' Convenience warning function '''
		message = message.format(*args)
		self.logger.warn('{} | {}'.format(self.address, message))

	def error(self, message, *args):
		''' Convenience error function '''
		message = message.format(*args)
		self.logger.error('{} | {}'.format(self.address, message))

	def exception(self, message, *args):
		''' Convenience exception function '''
		message = message.format(*args)
		self.logger.exception('{} | {}'.format(self.address, message))

	def handle(self):
		''' Handle connection '''
		self.debug('Handle')
		raise NotImplementedError

	def finish(self):
		''' Finish connection by flushing stream, shutting down socket, and
		then closing it '''
		self.debug('Finish')
		try:
			self.stream.flush()
			self.socket.shutdown(socket.SHUT_RDWR)
		except socket.error as e:
			pass    # Ignore socket errors
		finally:
			self.socket.close()

# HTTP Handler

class HTTPHandler(BaseHandler):
	def __init__(self, fd, address, docroot=DOCROOT):
		BaseHandler.__init__(self, fd, address)
		self.fd = fd
		self.address = adress
		self.docroot = docroot

	def handle(self):
		data = self.stream.readline().rstrip()
		while data:
			sys.stdout.write(data + '\n')
			data = self.stream.readline().rstrip()

		self.stream.write('HTTP/1.0 200 OK\r\n')
		self.stream.write('Content-Type: text/html\r\n')
		self.stream.write('\r\n')
		'''
		self.stream.write('<h1>Hello, {}:{}</h1>'.format(self.address[0], self.address[1]))
		'''
		self._handle_file()

		os.environ['REMOTE_ADDR'] = self.address.split(':', 1)[0]
		os.environ['REMOTE_HOST'] = self.address.split(':', 1)[0]
		os.environ['REMOTE_PORT'] = self.address.split(':', 1)[1]

		data = self.stream.readline().strip().split()
		print data[0]
		os.environ['REQUEST_METHOD'] = data[0]
		os.environ['REQUEST_URI'] = data[1]
		os.environ['QUERY_STRING'] = data[1].split('?')[-2]
	
		# Build uripath by normalizing REQUEST_URI
		self.uripath = os.path.normpath(self.docroot + os.environ['REQUEST_URI'])
	
		# Check path existence and types and then dispatch
		if not self.uripath:
			self._handle_error(404) # 404 error
		elif os.path.isfile(self.uripath) and os.access(self.uripath, os.R_OK):
			self._handle_script()   # CGI script
		elif is_file(self.uripath) and os.access(self.uripath, os.R_OK):
			self._handle_file()     # Static file
		elif os.path.isdir(self.uripath) and os.access(self.uripath, os.R_OK):
			self._handle_directory()# Directory listing
		else:
			self._handler_error(403)# 403 error

	def _handle_file(self):
		self.stream.write('<pre>')
		for line in open(__file__):
			self.stream.write(line)
		self.stream.write('</pre>')
		
# TCPServer class

class TCPServer(object):
	def __init__(self, address=ADDRESS, port=PORT, handler=HTTPHandler):
		self.logger  = logging.getLogger()
		self.address = socket.gethostbyname(address)
		self.port    = port
		self.socket  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.handler = handler

	def run(self):
		try:
	 		self.socket.bind((self.address, self.port))
	  		self.socket.listen(0)
		except socket.error as e:
			self.logger.error('Could not listen on {}:{}: {}'.format(self.address, self.port, e))
			sys.exit(1)
		self.logger.info('Listening on {}:{}...'.format(self.address, self.port))
		
		while True:
			client, address = self.socket.accept()
			self.logger.debug('Accepted connection from {}:{}'.format(*address))
			try:
				handler = self.handler(client, address)
			  	handler.handle()
			except Exception as e:
			   	handler.exception('Exception: {}', e)
			finally:
			  	handler.finish()

# Main execution

if __name__ == '__main__':
	# Parse command-line arguments
	try:
		options, arguments = getopt.getopt(sys.argv[1:], "hd:p:fv")
	except getopt.GetoptError as e:
		print e
		usage(1)

	for opts, args in options:
		if opts == '-d':
			DOCROOT = args
		if opts == '-p':
			PORT = int(args)
		if opts == '-f':
			FORK = True
		if opts == '-v':
			LOGLEVEL = logging.DEBUG
		
	# Set logging level
	logging.basicConfig(
		level   = LOGLEVEL,
		format  = '[%(asctime)s] %(message)s',
		datefmt = '%Y-%m-%d %H:%M:%S',
	)

	# Instantiate and run server
	server = TCPServer(ADDRESS, PORT, HTTPHandler)

	try:
		server.run()
	except KeyboardInterrupt:
		sys.exit(0)

