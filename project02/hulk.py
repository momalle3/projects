#!/usr/bin/env python2.7

import hashlib
import string
import sys
import itertools
import getopt

# Constants

ALPHABET = string.ascii_lowercase + string.digits 
LENGTH = 8
HASHES = 'hashes.txt'
PREFIX = ''

# Utility Function

def usage(exit_code=0):
	print >>sys.stderr, '''Usage: hulk.py [-a ALPHABET -l LENGTH -s HASHES -p PREFIX]

Options:

      -a  ALPHABET    Alphabet used for passwords
      -l  LENGTH      Length for passwords
      -s  HASHES      Path to file containing hashes
      -p  PREFIX      Prefix to use for each candidate password
'''
	sys.exit(exit_code)

def md5sum(s):
	return hashlib.md5(s).hexdigest()
	
# Main Execution

if __name__ == '__main__':
	try:
		opts, args = getopt.getopt(sys.argv[1:],'a:l:s:p:')
	except getopt.GetoptError as e:
		print e
		usage(1)

	for o, a in opts:
		if o == '-a':
			ALPHABET = a
		if o == '-l':
			LENGTH = int(a)
		if o == '-s':
			HASHES = a
		if o == '-p':
			PREFIX = a
	
	hashes = set([l.strip() for l in open(HASHES)])
	
	for candidate in itertools.product(ALPHABET, repeat=LENGTH):
		candidate = ''.join(candidate)
		candidate = PREFIX + candidate
		checksum = md5sum(candidate)
		if checksum in hashes:
			print candidate
	
