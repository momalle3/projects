#!/usr/bin/env python2.7

import sys
import work_queue
import json
import subprocess

# Constants

LENGTH = 8
HASHES = 'hashes.txt'
TASKS = list()
SOURCES = ('hulk.py', HASHES)
PORT = 9776
try:
	JOURNAL = json.load(open("jounal.json"))
except:
	JOURNAL = {}

# Prefix Function loads commands with length 5 and varying prefixes

def prefix():
	one = subprocess.Popen("./hulk.py -l 1", stdout=subprocess.PIPE, shell=True)
	prefix1 = one.communicate()[0].split()
	two = subprocess.Popen("./hulk.py -l 2", stdout=subprocess.PIPE, shell=True)
	prefix2 = two.communicate()[0].split()
	three = subprocess.Popen("./hulk.py -l 3", stdout=subprocess.PIPE, shell=True)
	prefix3 = three.communicate()[0].split()
	for line in prefix1:
		TASKS.append('./hulk.py -l 5 -p {}'.format(line))
	for line in prefix2:
		TASKS.append('./hulk.py -l 5 -p {}'.format(line))
	for line in prefix3:
		TASKS.append('./hulk.py -l 5 -p {}'.format(line))

# Main Execution

if __name__ == '__main__':
	queue = work_queue.WorkQueue(PORT, name='fury-momalle3', catalog=True)
	queue.specify_log('fury.log')
	
	TASKS=(['./hulk.py -l {}'.format(length) for length in range(LENGTH-3)])
	prefix()
	
	for command in TASKS:
		if command in JOURNAL:
   			print >>sys.stderr, 'Already did', command
		else:
			task = work_queue.Task(command)
				
			for source in SOURCES:
				task.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)
		
		queue.submit(task)
		
	while not queue.empty():
		task = queue.wait()
		if task and task.return_status == 0:
			JOURNAL[task.command] = task.output.split()
			with open('journal.json.new', 'w') as stream:
				json.dump(JOURNAL, stream)
			os.rename('journal.json.new', 'journal.json')
