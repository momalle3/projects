Project 02: Distributed Computing
=================================

Team Member: Michael O'Malley

1. Hulk cracks password by taking an alphabet and a list
password hashes. It first iterates through the alphabet
and creates candidates according to a specified length.
Then it adds a prefix and checks if any candidates are 
in the password hashes list. If they are, then the 
cracked password is printed. This program was tested
by trying many different combinations or flags on a 
smaller hashes text. If the program returned exactly
what was being asked, then it was successful. It did 
this every time.

2. Fury utilizes the hulk program to crack passwords of
large length. While the hulk program can do this, it
takes far too long. Fury calls workers into a queue to 
give them a task. It assigns a task to each worker and
allows them to complete it. Once their task is done, the
outputs along with the command are logged. This is done 
with a JOURNAL. This journal is checked every time before
assigning a task; if the task has been done, that task is
skipped. To check for correctness, I first made sure my
script was building my command list correctly, so I printed
that and found out that it was. Next, I ran fury and 
checked if workers were called and working. Once I knew
they were, I knew fury was working properly.

3. From my own experience, longer passwords are more
difficult to brute force. Each extra character adds 
exponentially more combinations and permutations to
find, adding significant length to the program run time.

